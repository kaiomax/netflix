package netflix.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import netflix.MainApp;
import netflix.domain.Video;

public class VideoOverviewController {

    @FXML
    private TableView<Video> videoTable;
    @FXML
    private TableColumn<Video, String> nameColumn;
	
    @FXML
    private Label nameLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private Label yearLabel;
    @FXML
    private Label minutesLabel;
    @FXML
    private Label categoryLabel;
    @FXML
    private Label directorLabel;
    @FXML
    private Label mainActorLabel;
    @FXML
    private Label ageGroupLabel;
    @FXML
    private TextField addressLabel;
    @FXML
    private ImageView posterImageView;
    
    // Reference to the main application.
    private MainApp mainApp;
    
    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public VideoOverviewController() {
    }
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Initialize the video table with the name column.
    	nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
    	
        // Clear video details.
        showVideoDetails(null);
        

        // Listen for selection changes and show the video details when changed.
        videoTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showVideoDetails(newValue));
    }
    
    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        videoTable.setItems(mainApp.getVideoData());
    }
    
    /**
     * Fills all text fields to show details about the video.
     * If the specified video is null, all text fields are cleared.
     * 
     * @param video the video or null
     */
    private void showVideoDetails(Video video) {
        if (video != null) {
            // Fill the labels with info from the video object.
            this.nameLabel.setText(video.getName());
            this.descriptionLabel.setText(video.getDescription());
        	this.yearLabel.setText(Integer.toString(video.getYear()));
        	this.minutesLabel.setText(Integer.toString(video.getMinutes()));
        	this.ageGroupLabel.setText(Integer.toString(video.getAgeGroup()));
        	this.categoryLabel.setText(video.getCategory().getName());
        	this.directorLabel.setText(video.getDirector().getName());
        	this.mainActorLabel.setText(video.getMainActor().getName());
        	this.addressLabel.setText(video.getAddress());
        	this.posterImageView.setImage(new Image(video.getThumbnail()));
        } else {
            // Video is null, remove all the text.
        	this.nameLabel.setText("");
        	this.descriptionLabel.setText("");
        	this.yearLabel.setText("");
        	this.minutesLabel.setText("");
        	this.ageGroupLabel.setText("");
        	this.categoryLabel.setText("");
        	this.directorLabel.setText("");
        	this.mainActorLabel.setText("");
        	this.addressLabel.setText("");
        	this.posterImageView.setImage(new Image("http://cdn.bulbagarden.net/upload/thumb/5/55/016Pidgey.png/250px-016Pidgey.png"));
        }
    }
    
    /**
     * Called when the user clicks the new button. Opens a dialog to edit
     * details for a new video.
     */
    @FXML
    private void handleNewVideo() {
    	Video tempVideo = new Video();
        boolean okClicked = mainApp.showVideoEditDialog(tempVideo);
        if (okClicked) {
        	mainApp.getVideoData().add(tempVideo);
        }
    }
    
    /**
     * Called when the user clicks the edit button. Opens a dialog to edit
     * details for the selected person.
     */
    @FXML
    private void handleEditVideo() {
    	Video selectedVideo = videoTable.getSelectionModel().getSelectedItem();
        if (selectedVideo != null) {
            boolean okClicked = mainApp.showVideoEditDialog(selectedVideo);
            if (okClicked) {
                showVideoDetails(selectedVideo);
            }

        } else {
        	showAlertNothingSelected();
        }
    }
    
    @FXML
    private void handlePlayVideo() {
    	Video selectedVideo = videoTable.getSelectionModel().getSelectedItem();
        if (selectedVideo != null) {
        	mainApp.playVideo(selectedVideo);
        } else {
        	showAlertNothingSelected();
        }
    }
    
    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void handleDeleteVideo() {
        int selectedIndex = videoTable.getSelectionModel().getSelectedIndex();
        
        if (selectedIndex >= 0) {
        	videoTable.getItems().remove(selectedIndex);
        } else {
        	showAlertNothingSelected();
        }
    }
    
    private void showAlertNothingSelected() {
        // Nothing selected.
        Alert alert = new Alert(AlertType.WARNING);
        alert.initOwner(mainApp.getPrimaryStage());
        alert.setTitle("Vídeo Inexistente");
        alert.setHeaderText("Nenhum vídeo foi selecionado");
        alert.setContentText("Por favor selecione algum vídeo na tabela.");

        alert.showAndWait();
    }
}
