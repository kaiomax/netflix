package netflix.view;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import netflix.MainApp;

public class RootLayoutController {
	@FXML
    private MenuItem userMenuItem;
	
	@FXML
    private MenuItem videoMenuItem;
	
	// Reference to the main application.
    private MainApp mainApp;
    
    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public RootLayoutController() {
    }
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }
    
    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    
    @FXML
    public void showUserOverview() {
    	this.mainApp.showUserOverview();
    }
    
    @FXML
    public void showVideoOverview() {
    	this.mainApp.showVideoOverview();
    }
}
