package netflix.view;

import java.io.File;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import netflix.domain.Actor;
import netflix.domain.Category;
import netflix.domain.Director;
import netflix.domain.Video;

public class VideoEditDialogController {

    @FXML
    private TextField nameField;
    @FXML
    private TextField yearField;
    @FXML
    private TextField minutesField;
    @FXML
    private TextField categoryField;
    @FXML
    private TextField directorField;
    @FXML
    private TextField mainActorField;
    @FXML
    private TextField ageGroupField;
    @FXML
    private TextField addressField;
    @FXML
    private TextField thumbnailField;
    
    @FXML
    private TextArea descriptionField;
	
    
    private Stage dialogStage;
    private Video video;
    private boolean okClicked = false;
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }
    
    /**
     * Sets the stage of this dialog.
     * 
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    /**
     * Sets the video to be edited in the dialog.
     * 
     * @param video
     */
    public void setVideo(Video video) {
        this.video = video;

        nameField.setText(video.getName());
        descriptionField.setText(video.getDescription());
    	yearField.setText(Integer.toString(video.getYear()));
    	minutesField.setText(Integer.toString(video.getMinutes()));
    	if(video.getCategory() != null) {
    		categoryField.setText(video.getCategory().getName());	
    	}
    	if(video.getDirector() != null) {
    		directorField.setText(video.getDirector().getName());
    	}
    	if(video.getMainActor() != null) {
    		mainActorField.setText(video.getMainActor().getName());
    	}
    	ageGroupField.setText(Integer.toString(video.getAgeGroup()));
    	addressField.setText(video.getAddress());
    	thumbnailField.setText(video.getThumbnail());
    }
    
    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }
    
    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
        	Category category = new Category();
        	category.setName(categoryField.getText());
        	Director director = new Director();
        	director.setName(directorField.getText());
        	Actor actor = new Actor();
        	actor.setName(mainActorField.getText());
        	
            video.setName(nameField.getText());
            video.setDescription(descriptionField.getText());
            video.setYear(Integer.parseInt(yearField.getText()));
            video.setMinutes(Integer.parseInt(minutesField.getText()));
            video.setCategory(category);
            video.setDirector(director);
            video.setMainActor(actor);
            video.setAgeGroup(Integer.parseInt(ageGroupField.getText()));
            video.setAddress(addressField.getText());
            video.setThumbnail(thumbnailField.getText());

            okClicked = true;
            dialogStage.close();
        }
    }
    
    /**
     * Called when the user clicks to open FileChooser.
     */
    @FXML
    private void handleOpenFileChooser() {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(
        		new ExtensionFilter("MP4 Files", "*.mp4"),
        		new ExtensionFilter("FLV Files", "*.flv"),
        		new ExtensionFilter("RMVB Files", "*.rmvb"),
        		new ExtensionFilter("MKV Files", "*.mkv"));
        File selectedFile = fc.showOpenDialog(null);
        
        if (selectedFile != null) {
            addressField.setText(selectedFile.getAbsolutePath());
        }
    }
    
    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
    
    /**
     * Validates the user input in the text fields.
     * 
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (nameField.getText() == null || nameField.getText().length() == 0) {
            errorMessage += "É necessário inserir o título!\n"; 
        }
        if (descriptionField.getText() == null || descriptionField.getText().length() == 0) {
            errorMessage += "É necessário inserir a descrição!\n"; 
        }
        if (yearField.getText() == null || yearField.getText().length() == 0) {
            errorMessage += "É necessário inserir o ano!\n"; 
        }
        if (minutesField.getText() == null || minutesField.getText().length() == 0) {
            errorMessage += "É necessário inserir a duração do filme em minutos!\n"; 
        }
        if (categoryField.getText() == null || categoryField.getText().length() == 0) {
            errorMessage += "É necessário inserir a categoria!\n"; 
        }
        if (directorField.getText() == null || directorField.getText().length() == 0) {
            errorMessage += "É necessário inserir o diretor!\n"; 
        }
        if (mainActorField.getText() == null || mainActorField.getText().length() == 0) {
            errorMessage += "É necessário inserir o ator principal!\n"; 
        }
        if (ageGroupField.getText() == null || ageGroupField.getText().length() == 0) {
            errorMessage += "É necessário inserir a faixa etária!\n"; 
        }
        if (addressField.getText() == null || addressField.getText().length() == 0) {
            errorMessage += "É necessário informar o caminho para o arquivo!\n"; 
        }
        if (thumbnailField.getText() == null || thumbnailField.getText().length() == 0) {
            errorMessage += "É necessário informar a url para o poster!\n"; 
        }


        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Formulário inválido");
            alert.setHeaderText("Por favor insira os campos corretamente.");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}
