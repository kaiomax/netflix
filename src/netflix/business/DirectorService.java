package netflix.business;

import java.sql.SQLException;
import java.util.List;

import netflix.dao.DirectorDao;
import netflix.domain.Director;

public class DirectorService {
	private DirectorDao catDao;
	
	public List<Director> list() {
		try {
			return catDao.list();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Director> search(String filter) {
		try {
			return catDao.search(filter);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Director get(int id) {
		try {
			return catDao.get(id);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void insert(Director director) {
		try {
			catDao.insert(director);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void delete(int id) {
		try {
			catDao.delete(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void update(Director director) {
		try {
			catDao.update(director);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
