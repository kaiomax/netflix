package netflix.business;

import java.sql.SQLException;
import java.util.List;

import netflix.dao.ActorDao;
import netflix.domain.Actor;

public class ActorService {
	private ActorDao actorDao;
	
	public List<Actor> list() {
		try {
			return actorDao.list();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Actor> search(String filter) {
		try {
			return actorDao.search(filter);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Actor get(int id) {
		try {
			return actorDao.get(id);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void insert(Actor actor) {
		try {
			actorDao.insert(actor);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void delete(int id) {
		try {
			actorDao.delete(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void update(Actor actor) {
		try {
			actorDao.update(actor);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
