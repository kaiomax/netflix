package netflix.business;

import java.sql.SQLException;
import java.util.List;

import netflix.dao.CategoryDao;
import netflix.domain.Category;

public class CategoryService {
	private CategoryDao catDao;
	
	public List<Category> list() {
		try {
			return catDao.list();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Category> search(String filter) {
		try {
			return catDao.search(filter);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Category get(int id) {
		try {
			return catDao.get(id);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void insert(Category category) {
		try {
			catDao.insert(category);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void delete(int id) {
		try {
			catDao.delete(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void update(Category category) {
		try {
			catDao.update(category);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
