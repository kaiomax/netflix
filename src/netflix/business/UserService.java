package netflix.business;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;

import netflix.dao.UserDao;
import netflix.domain.User;

public class UserService {
	UserDao userDao = new UserDao();
	
	public User login(String login, String password) {
		password = hashCode(password);
		
		try {
			return userDao.login(login, password);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<User> list() {
		try {
			return userDao.list();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<User> search(String filter) {
		try {
			return userDao.search(filter);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public User get(int id) {
		try {
			return userDao.get(id);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void insert(User usr) {
		usr.setPassword(hashCode(usr.getPassword()));
		try {
			userDao.insert(usr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void delete(int id) {
		try {
			userDao.delete(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void update(User usr) {
		try {
			userDao.update(usr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	private String hashCode(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			
			md.update(input.getBytes(), 0, input.length());
			
			return new BigInteger(1, md.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return input;
		}
	}
}
