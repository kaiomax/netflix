package netflix.business;

import java.sql.SQLException;
import java.util.List;

import netflix.dao.VideoDao;
import netflix.domain.Video;

public class VideoService {
	private VideoDao vidDao = new VideoDao();
	
	public List<Video> list(int age) {
		try {
			List<Video> videos = vidDao.list();
			
			for (Video video : videos) {
				if(video.getAgeGroup() > age) {
					videos.remove(video);
				}
			}
			
			return videos;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Video> list() {
		try {
			return vidDao.list();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Video> search(String filter) {
		try {
			return vidDao.search(filter);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Video> search(String filter, int age) {
		try {
			List<Video> videos = vidDao.search(filter);
			
			for (Video video : videos) {
				if(video.getAgeGroup() > age) {
					videos.remove(video);
				}
			}
			
			return videos;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Video get(int id) {
		try {
			return vidDao.get(id);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void insert(Video video) {
		try {
			vidDao.insert(video);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void delete(int id) {
		try {
			vidDao.delete(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void update(Video video) {
		try {
			vidDao.update(video);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
