package netflix.business;

import java.sql.SQLException;
import java.util.List;

import netflix.dao.FavoriteDao;
import netflix.domain.Favorite;

public class FavoriteService {
	FavoriteDao favDao = new FavoriteDao();
	
	public List<Favorite> listByUser(int id) {
		try {
			return favDao.listByUser(id);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Favorite> listByVideo(int id) {
		try {
			return favDao.listByVideo(id);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void insert(Favorite fav) {
		try {
			favDao.insert(fav);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void delete(int id) {
		try {
			favDao.delete(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
