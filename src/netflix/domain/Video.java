package netflix.domain;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Video {
	private int id;
	private String name;
	private String description;
	private int year;
	private int minutes;
	private int ageGroup;
	private int season;
	private int episode;
	private String address;
	private String thumbnail;
	private Actor mainActor;
	private Director director;
	private Category category;
	
	private final StringProperty nameProperty = new SimpleStringProperty();
	private final StringProperty descriptionProperty = new SimpleStringProperty();
	private final StringProperty addressProperty = new SimpleStringProperty();
	private final StringProperty thumbnailProperty = new SimpleStringProperty();
	private final IntegerProperty yearProperty = new SimpleIntegerProperty();
	private final IntegerProperty minutesProperty = new SimpleIntegerProperty();
	private final IntegerProperty ageGroupProperty = new SimpleIntegerProperty();
	private final ObjectProperty<Category> categoryProperty = new SimpleObjectProperty<Category>();
	private final ObjectProperty<Director> directorProperty = new SimpleObjectProperty<Director>();
	private final ObjectProperty<Actor> mainActorProperty = new SimpleObjectProperty<Actor>();
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	
    public Video() {
    }
    
    public Video(String name, String description, int year, int minutes, int ageGroup, Category category, Director director, Actor mainActor, String address, String thumbnail) {
    	this.name = name;
        this.nameProperty.set(name);
    	this.description = description;
        this.descriptionProperty.set(description);
        this.year = year;
        this.yearProperty.set(year);
        this.minutes = minutes;
        this.minutesProperty.set(minutes);
        this.ageGroup = ageGroup;
        this.ageGroupProperty.set(ageGroup);
        this.category = category;
        this.categoryProperty.set(category);
        this.director = director;
        this.directorProperty.set(director);
        this.mainActor = mainActor;
        this.mainActorProperty.set(mainActor);
        this.address = address;
        this.addressProperty.set(address);
        this.thumbnail = thumbnail;
        this.thumbnailProperty.set(thumbnail);
    }
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return nameProperty.get();
	}
	public void setName(String name) {
		this.nameProperty.set(name);
	}
    public StringProperty nameProperty() {
        return nameProperty;
    }
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
		this.descriptionProperty.set(name);
	}
    public StringProperty descriptionProperty() {
        return descriptionProperty;
    }
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
		this.yearProperty.set(year);
	}
    public IntegerProperty yearProperty() {
        return yearProperty;
    }
	public int getMinutes() {
		return minutes;
	}
	public void setMinutes(int minutes) {
		this.minutes = minutes;
		this.minutesProperty.set(minutes);
	}
    public IntegerProperty minutesProperty() {
        return minutesProperty;
    }
	public int getAgeGroup() {
		return ageGroup;
	}
	public void setAgeGroup(int ageGroup) {
		this.ageGroup = ageGroup;
		this.ageGroupProperty.set(ageGroup);
	}
    public IntegerProperty ageGroupProperty() {
        return ageGroupProperty;
    }
	public int getSeason() {
		return season;
	}
	public void setSeason(int season) {
		this.season = season;
	}
	public int getEpisode() {
		return episode;
	}
	public void setEpisode(int episode) {
		this.episode = episode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
		this.addressProperty.set(address);
	}
    public StringProperty addressProperty() {
        return addressProperty;
    }
	public Actor getMainActor() {
		return mainActor;
	}
	public void setMainActor(Actor mainActor) {
		this.mainActor = mainActor;
		this.mainActorProperty.set(mainActor);
	}
	public ObjectProperty<Actor> mainActorProperty() {
		return mainActorProperty;
	}
	public Director getDirector() {
		return director;
	}
	public void setDirector(Director director) {
		this.director = director;
		this.directorProperty.set(director);
	}
	public ObjectProperty<Director> directorProperty() {
		return directorProperty;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
		this.categoryProperty.set(category);
	}
	public ObjectProperty<Category> categoryProperty() {
	  return categoryProperty;
  	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
    public StringProperty thumbnailProperty() {
        return thumbnailProperty;
    }
}
