package netflix.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import netflix.domain.Category;

public class CategoryDao extends DataBase {
	
	@SuppressWarnings("finally")
	public List<Category> list() throws SQLException {
		Connection conn = null;
		Statement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.createStatement();
			rs = stt.executeQuery("SELECT * FROM Category");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			List<Category> categories = this.fillList(rs);
			rs.close();
			stt.close();
			conn.close();
			return categories;
		}
	}
	
	@SuppressWarnings("finally")
	public List<Category> search(String filter) throws SQLException {
		filter = "%" + filter + "%";
		
		Connection conn = null;
		PreparedStatement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("SELECT * FROM Category c WHERE c.name LIKE ? or c.description LIKE ?");
			stt.setString(1, filter);
			stt.setString(2, filter);
			rs = stt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			List<Category> categories = this.fillList(rs);
			rs.close();
			stt.close();
			conn.close();
			return categories;
		}
	}
	
	@SuppressWarnings("finally")
	public Category get(int id) throws SQLException {		
		Connection conn = null;
		PreparedStatement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("SELECT * FROM Category c WHERE c.id = ?");
			stt.setInt(1, id);
			rs = stt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			rs.next();
			Category category = new Category();
			category.setId(rs.getInt("id"));
			category.setDescription(rs.getString("description"));
			category.setName(rs.getString("name"));
			
			rs.close();
			stt.close();
			conn.close();
			return category;
		}
	}
	
	public void insert(Category category) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("INSERT INTO Category (name, description) VALUES (?,?)");
			stt.setString(1, category.getName());
			stt.setString(2, category.getDescription());
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	public void delete(int id) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("DELETE FROM Categoty c WHERE c.id = ?");
			stt.setInt(1, id);
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	public void update(Category category) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("UPDATE Category SET name = ?, description = ? WHERE id = ?");
			stt.setString(1, category.getName());
			stt.setString(2, category.getDescription());
			stt.setInt(3, category.getId());
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	private List<Category> fillList(ResultSet rs) {
		ArrayList<Category> categories = new ArrayList<Category>();
		
		try {
			while(rs.next()){
				Category category = new Category();
				category.setId(rs.getInt("id"));
				category.setName(rs.getString("name"));
				category.setDescription(rs.getString("description"));
				
				categories.add(category);
			}
			return categories;
		} catch (SQLException e) {
			return null;
		}
	}
}
