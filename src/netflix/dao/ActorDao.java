package netflix.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import netflix.domain.Actor;

public class ActorDao extends DataBase {
	
	@SuppressWarnings("finally")
	public List<Actor> list() throws SQLException {
		Connection conn = null;
		Statement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.createStatement();
			rs = stt.executeQuery("SELECT * FROM Actor");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			List<Actor> actors = this.fillList(rs);
			rs.close();
			stt.close();
			conn.close();
			return actors;
		}
	}
	
	@SuppressWarnings("finally")
	public List<Actor> search(String filter) throws SQLException {
		filter = "%" + filter + "%";
		
		Connection conn = null;
		PreparedStatement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("SELECT * FROM Actor a WHERE a.name LIKE ? or a.info LIKE ?");
			stt.setString(1, filter);
			stt.setString(2, filter);
			rs = stt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			List<Actor> actors = this.fillList(rs);
			rs.close();
			stt.close();
			conn.close();
			return actors;
		}
	}
	
	@SuppressWarnings("finally")
	public Actor get(int id) throws SQLException {		
		Connection conn = null;
		PreparedStatement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("SELECT * FROM Actor a WHERE a.id = ?");
			stt.setInt(1, id);
			rs = stt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			rs.next();
			Actor actor = new Actor();
			actor.setId(rs.getInt("id"));
			actor.setInfo(rs.getString("info"));
			actor.setName(rs.getString("name"));
			
			rs.close();
			stt.close();
			conn.close();
			return actor;
		}
	}
	
	public void insert(Actor actor) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("INSERT INTO Actor (name, info) VALUES (?,?)");
			stt.setString(1, actor.getName());
			stt.setString(2, actor.getInfo());
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	public void delete(int id) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("DELETE FROM Actor a WHERE a.id = ?");
			stt.setInt(1, id);
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	public void update(Actor actor) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("UPDATE Actor SET name = ?, info = ? WHERE id = ?");
			stt.setString(1, actor.getName());
			stt.setString(2, actor.getInfo());
			stt.setInt(3, actor.getId());
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	private List<Actor> fillList(ResultSet rs) {
		ArrayList<Actor> actors = new ArrayList<Actor>();
		
		try {
			while(rs.next()){
				Actor actor = new Actor();
				actor.setId(rs.getInt("id"));
				actor.setName(rs.getString("name"));
				actor.setInfo(rs.getString("info"));
				
				actors.add(actor);
			}
			return actors;
		} catch (SQLException e) {
			return null;
		}
	}
}
