package netflix.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import netflix.domain.Video;


public class VideoDao extends DataBase {
	
	@SuppressWarnings("finally")
	public List<Video> list() throws SQLException {
		Connection conn = null;
		Statement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.createStatement();
			rs = stt.executeQuery("SELECT * FROM Video");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			List<Video> videos = this.fillList(rs);
			rs.close();
			stt.close();
			conn.close();
			return videos;
		}
	}
	
	@SuppressWarnings("finally")
	public List<Video> search(String filter) throws SQLException {
		filter = "%" + filter + "%";
		
		Connection conn = null;
		PreparedStatement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("SELECT * FROM Video v "
					+ "JOIN Actor a ON v.main_actor = a.id "
					+ "JOIN Category c ON v.category = c.id "
					+ "WHERE v.name LIKE ? OR v.description LIKE ? OR "
					+ "c.name LIKE ? OR c.description LIKE ? OR"
					+ "a.name LIKE ? OR a.info LIKE ?");
			stt.setString(1, filter);
			stt.setString(2, filter);
			stt.setString(3, filter);
			stt.setString(4, filter);
			stt.setString(5, filter);
			stt.setString(6, filter);
			
			rs = stt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			List<Video> videos = this.fillList(rs);
			rs.close();
			stt.close();
			conn.close();
			return videos;
		}
	}
	
	@SuppressWarnings("finally")
	public Video get(int id) throws SQLException {		
		Connection conn = null;
		PreparedStatement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("SELECT * FROM Video v WHERE v.id = ?");
			stt.setInt(1, id);
			rs = stt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			rs.next();
			Video video = new Video();
			video.setId(rs.getInt("id"));
			video.setName(rs.getString("name"));
			video.setAddress(rs.getString("address"));
			video.setAgeGroup(rs.getInt("age_group"));
			video.setDescription(rs.getString("description"));
			video.setEpisode(rs.getInt("episode"));
			video.setMinutes(rs.getInt("minutes"));
			video.setSeason(rs.getInt("season"));
			video.setYear(rs.getInt("year"));
			video.setThumbnail(rs.getString("thumbnail"));
			
			DirectorDao dir = new DirectorDao();
			video.setDirector(dir.get(rs.getInt("director")));
			
			CategoryDao cat = new CategoryDao();
			video.setCategory(cat.get(rs.getInt("category")));
			
			ActorDao act = new ActorDao();
			video.setMainActor(act.get(rs.getInt("main_actor")));
			
			rs.close();
			stt.close();
			conn.close();
			return video;
		}
	}
	
	public void insert(Video video) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("INSERT INTO Video (name, description, year, minutes, age_group, season, episode,"
					+ " address, thumbnail, main_actor, director, category) "
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
			stt.setString(1, video.getName());
			stt.setString(2, video.getDescription());
			stt.setInt(3, video.getYear());
			stt.setInt(4, video.getMinutes());
			stt.setInt(5, video.getAgeGroup());
			stt.setInt(6, video.getSeason());
			stt.setInt(7, video.getEpisode());
			stt.setString(8, video.getAddress());
			stt.setString(8, video.getThumbnail());
			stt.setInt(9, video.getMainActor().getId());
			stt.setInt(10, video.getDirector().getId());
			stt.setInt(11, video.getCategory().getId());
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	public void delete(int id) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("DELETE FROM Video v WHERE v.id = ?");
			stt.setInt(1, id);
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	public void update(Video video) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("UPDATE Video SET name = ?, description = ?, year = ?, minutes = ?, age_group = ?, season = ?, episode = ?,"
					+ " address = ?, main_actor = ?, director = ?, category = ?, thumbnail = ?)");
			stt.setString(1, video.getName());
			stt.setString(2, video.getDescription());
			stt.setInt(3, video.getYear());
			stt.setInt(4, video.getMinutes());
			stt.setInt(5, video.getAgeGroup());
			stt.setInt(6, video.getSeason());
			stt.setInt(7, video.getEpisode());
			stt.setString(8, video.getAddress());
			stt.setInt(9, video.getMainActor().getId());
			stt.setInt(10, video.getDirector().getId());
			stt.setInt(11, video.getCategory().getId());
			stt.setString(12, video.getThumbnail());
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	private List<Video> fillList(ResultSet rs) {
		ArrayList<Video> videos = new ArrayList<Video>();
		
		try {
			while(rs.next()){
				Video video = new Video();
				video.setId(rs.getInt("id"));
				video.setName(rs.getString("name"));
				video.setAddress(rs.getString("address"));
				video.setAgeGroup(rs.getInt("age_group"));
				video.setDescription(rs.getString("description"));
				video.setEpisode(rs.getInt("episode"));
				video.setMinutes(rs.getInt("minutes"));
				video.setSeason(rs.getInt("season"));
				video.setYear(rs.getInt("year"));
				video.setThumbnail(rs.getString("thumbnail"));
				
				DirectorDao dir = new DirectorDao();
				video.setDirector(dir.get(rs.getInt("director")));
				
				CategoryDao cat = new CategoryDao();
				video.setCategory(cat.get(rs.getInt("category")));
				
				ActorDao act = new ActorDao();
				video.setMainActor(act.get(rs.getInt("main_actor")));
				
				videos.add(video);
			}
			return videos;
		} catch (SQLException e) {
			return null;
		}
	}
}
