package netflix.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import netflix.domain.User;

public class UserDao extends DataBase{
	
	@SuppressWarnings("finally")
	public User login(String login, String password) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("SELECT * FROM Users u WHERE u.login = ? AND u.password = ?");
			stt.setString(1, login);
			stt.setString(2, password);
			rs = stt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			rs.next();
			User usr = new User();
			usr.setAdmin(rs.getBoolean("admin"));
			usr.setBirthDate(rs.getDate("birth_date"));
			usr.setId(rs.getInt("id"));
			usr.setLogin(rs.getString("login"));
			usr.setName(rs.getString("name"));
			usr.setPassword(rs.getString("password"));
			
			rs.close();
			stt.close();
			conn.close();
			return usr;
		}
	}
	
	
	@SuppressWarnings("finally")
	public List<User> list() throws SQLException {
		Connection conn = null;
		Statement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.createStatement();
			rs = stt.executeQuery("SELECT * FROM Users");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			List<User> users = this.fillList(rs);
			rs.close();
			stt.close();
			conn.close();
			return users;
		}
	}
	
	@SuppressWarnings("finally")
	public List<User> search(String filter) throws SQLException {
		filter = "%" + filter + "%";
		
		Connection conn = null;
		PreparedStatement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("SELECT * FROM Users u WHERE u.name LIKE ? or u.login LIKE ?");
			stt.setString(1, filter);
			stt.setString(2, filter);
			rs = stt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			List<User> users = this.fillList(rs);
			rs.close();
			stt.close();
			conn.close();
			return users;
		}
	}
	
	@SuppressWarnings("finally")
	public User get(int id) throws SQLException {		
		Connection conn = null;
		PreparedStatement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("SELECT * FROM Users u WHERE u.id = ?");
			stt.setInt(1, id);
			rs = stt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			rs.next();
			User usr = new User();
			usr.setAdmin(rs.getBoolean("admin"));
			usr.setBirthDate(rs.getDate("birth_date"));
			usr.setId(rs.getInt("id"));
			usr.setLogin(rs.getString("login"));
			usr.setName(rs.getString("name"));
			usr.setPassword(rs.getString("password"));
			
			rs.close();
			stt.close();
			conn.close();
			return usr;
		}
	}
	
	public void insert(User usr) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("INSERT INTO Users (name, admin, password, login, birth_date) VALUES (?,?,?,?,?)");
			stt.setString(1, usr.getName());
			stt.setBoolean(2, usr.isAdmin());
			stt.setString(3, usr.getPassword());
			stt.setString(4, usr.getLogin());
			stt.setDate(5, (Date) usr.getBirthDate());
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	public void delete(int id) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("DELETE FROM Users u WHERE u.id = ?");
			stt.setInt(1, id);
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	public void update(User usr) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("UPDATE Users SET name = ?, admin = ?, password = ?, login = ?, birth_date = ? WHERE id = ?");
			stt.setString(1, usr.getName());
			stt.setBoolean(2, usr.isAdmin());
			stt.setString(3, usr.getPassword());
			stt.setString(4, usr.getLogin());
			stt.setDate(5, (Date) usr.getBirthDate());
			stt.setInt(6, usr.getId());
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	private List<User> fillList(ResultSet rs) {
		ArrayList<User> users = new ArrayList<User>();
		
		try {
			while(rs.next()){
				User usr = new User();
				usr.setAdmin(rs.getBoolean("admin"));
				usr.setBirthDate(rs.getDate("birth_date"));
				usr.setId(rs.getInt("id"));
				usr.setLogin(rs.getString("login"));
				usr.setName(rs.getString("name"));
				usr.setPassword(rs.getString("password"));
				
				users.add(usr);
			}
			return users;
		} catch (SQLException e) {
			return null;
		}
	}
}
