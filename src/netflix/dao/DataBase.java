package netflix.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBase {	
	public DataBase() {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	protected Connection getConnection() {
		try {
			return DriverManager.getConnection("jdbc:postgresql://localhost:5432/Netflix", "postgres", "postgres");
		} catch (SQLException e) {
			return null;
		}
	}
}
