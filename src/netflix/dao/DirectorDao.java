package netflix.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import netflix.domain.Director;


public class DirectorDao extends DataBase {
	@SuppressWarnings("finally")
	public List<Director> list() throws SQLException {
		Connection conn = null;
		Statement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.createStatement();
			rs = stt.executeQuery("SELECT * FROM Director");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			List<Director> directors = this.fillList(rs);
			rs.close();
			stt.close();
			conn.close();
			return directors;
		}
	}
	
	@SuppressWarnings("finally")
	public List<Director> search(String filter) throws SQLException {
		filter = "%" + filter + "%";
		
		Connection conn = null;
		PreparedStatement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("SELECT * FROM Director d WHERE d.name LIKE ? or d.info LIKE ?");
			stt.setString(1, filter);
			stt.setString(2, filter);
			rs = stt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			List<Director> directors = this.fillList(rs);
			rs.close();
			stt.close();
			conn.close();
			return directors;
		}
	}
	
	@SuppressWarnings("finally")
	public Director get(int id) throws SQLException {		
		Connection conn = null;
		PreparedStatement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("SELECT * FROM Director d WHERE d.id = ?");
			stt.setInt(1, id);
			rs = stt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			rs.next();
			Director director = new Director();
			director.setId(rs.getInt("id"));
			director.setInfo(rs.getString("info"));
			director.setName(rs.getString("name"));
			
			rs.close();
			stt.close();
			conn.close();
			return director;
		}
	}
	
	public void insert(Director director) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("INSERT INTO Director (name, info) VALUES (?,?)");
			stt.setString(1, director.getName());
			stt.setString(2, director.getInfo());
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	public void delete(int id) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("DELETE FROM Director d WHERE d.id = ?");
			stt.setInt(1, id);
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	public void update(Director director) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("UPDATE Director SET name = ?, info = ? WHERE id = ?");
			stt.setString(1, director.getName());
			stt.setString(2, director.getInfo());
			stt.setInt(3, director.getId());
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	private List<Director> fillList(ResultSet rs) {
		ArrayList<Director> directors = new ArrayList<Director>();
		
		try {
			while(rs.next()){
				Director director = new Director();
				director.setId(rs.getInt("id"));
				director.setName(rs.getString("name"));
				director.setInfo(rs.getString("info"));
				
				directors.add(director);
			}
			return directors;
		} catch (SQLException e) {
			return null;
		}
	}
}
