package netflix.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import netflix.domain.Favorite;

public class FavoriteDao extends DataBase {
	
	@SuppressWarnings("finally")
	public List<Favorite> listByUser(int id) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("SELECT * FROM Favorite where user_id = ?");
			stt.setInt(1, id);
			rs = stt.executeQuery();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			List<Favorite> favorites = this.fillList(rs);
			rs.close();
			stt.close();
			conn.close();
			return favorites;
		}
	}
	
	@SuppressWarnings("finally")
	public List<Favorite> listByVideo(int id) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		ResultSet rs = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("SELECT * FROM Favorite where video_id = ?");
			stt.setInt(1, id);
			rs = stt.executeQuery("SELECT * FROM Favorite where video_id = ?");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			List<Favorite> favorites = this.fillList(rs);
			rs.close();
			stt.close();
			conn.close();
			return favorites;
		}
	}
	
	public void insert(Favorite fav) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("INSERT INTO Favorite (user_id, video_id) VALUES (?,?)");
			stt.setInt(1, fav.getUser().getId());
			stt.setInt(2, fav.getVideo().getId());
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	public void delete(int id) throws SQLException {
		Connection conn = null;
		PreparedStatement stt = null;
		try {
			conn = super.getConnection();
			stt = conn.prepareStatement("DELETE FROM Favorite f WHERE f.id = ?");
			stt.setInt(1, id);
			
			stt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			stt.close();
			conn.close();
		}
	}
	
	private List<Favorite> fillList(ResultSet rs) {
		ArrayList<Favorite> favorites = new ArrayList<Favorite>();
		
		try {
			while(rs.next()){
				Favorite fav = new Favorite();
				fav.setId(rs.getInt("id"));
				
				UserDao userDao = new UserDao();
				fav.setUser(userDao.get(rs.getInt("user_id")));
				
				VideoDao videoDao = new VideoDao();
				fav.setVideo(videoDao.get(rs.getInt("video_id")));
				
				favorites.add(fav);
			}
			return favorites;
		} catch (SQLException e) {
			return null;
		}
	}
}
