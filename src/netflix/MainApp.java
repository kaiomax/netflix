package netflix;

import java.io.File;
import java.io.IOException;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import netflix.domain.Actor;
import netflix.domain.Category;
import netflix.domain.Director;
import netflix.domain.Video;
import netflix.view.ManageUsersController;
import netflix.view.RootLayoutController;
import netflix.view.UserOverviewController;
import netflix.view.VideoEditDialogController;
import netflix.view.VideoOverviewController;

public class MainApp extends Application {
	
    private Stage primaryStage;
    private BorderPane rootLayout;
    private ObservableList<Video> videoList = FXCollections.observableArrayList();
    
    /**
     * Constructor
     */
    public MainApp() {
        // Sample data
    	videoList.add(new Video(
    			"Pulp Fiction", "Um filme de Quentin Tarantino!",
    			1994, 154, 18,
    			new Category("Crime"), new Director("Quentin Tarantino"), new Actor("John Travolta"), "//path",
    			"https://images-na.ssl-images-amazon.com/images/M/MV5BMTkxMTA5OTAzMl5BMl5BanBnXkFtZTgwNjA5MDc3NjE@._V1_SX300.jpg"));
    	videoList.add(new Video(
    			"Os Suspeitos", "A sole survivor tells of the twisty events leading up to a horrific gun battle on a boat, which begin when five criminals meet at a seemingly random police lineup.",
    			1995, 106, 14,
    			new Category("Crime"), new Director("Bryan Singer"), new Actor("Kevin Spacey"), "//path",
    			"https://images-na.ssl-images-amazon.com/images/M/MV5BMzI1MjI5MDQyOV5BMl5BanBnXkFtZTcwNzE4Mjg3NA@@._V1_SX300.jpg"));
    }

	@Override
	public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Netflix");

        initRootLayout();

        showVideoOverview();
	}
	
    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
            
            RootLayoutController controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Shows the video overview inside the root layout.
     */
    public void showVideoOverview() {
        try {
            // Load video overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/VideoOverview.fxml"));
            AnchorPane videoOverview = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(videoOverview);
            
            // Give the controller access to the main app.
            VideoOverviewController controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void showUserOverview() {
        try {
            // Load video overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/UserOverview.fxml"));
            AnchorPane userOverview = (AnchorPane) loader.load();

            rootLayout.setCenter(userOverview);
            
            // Give the controller access to the main app.
            UserOverviewController controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Opens a dialog to edit details for the specified video. If the user
     * clicks OK, the changes are saved into the provided video object and true
     * is returned.
     * 
     * @param video the video object to be edited
     * @return true if the user clicked OK, false otherwise.
     */
    public boolean showVideoEditDialog(Video video) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/VideoEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Editar Vídeo");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the video into the controller.
            VideoEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setVideo(video);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Opens a dialog to edit details for the specified video. If the user
     * clicks OK, the changes are saved into the provided video object and true
     * is returned.
     * 
     * @param video the video object to be edited
     * @return true if the user clicked OK, false otherwise.
     */
    public void playVideo(Video video) {
        Group root = new Group();
        
        File file = new File(video.getAddress());
        String path = file.toURI().toASCIIString();
        
        Media media = new Media(path);
        final MediaPlayer player = new MediaPlayer(media);
        MediaView view = new MediaView(player);
        
        final VBox vbox = new VBox();
        final HBox hbox = new HBox();
        final Slider slider = new Slider();
        Button pauseButton = new Button("Pause");
        Button playButton = new Button("Play");
        Button stopButton = new Button("Stop");
        vbox.getChildren().addAll(slider);
        hbox.getChildren().addAll(pauseButton, playButton, stopButton);
        hbox.setPadding(new Insets(15, 15, 15, 15));
        
        root.getChildren().addAll(view, vbox, hbox);
        
        Scene scene = new Scene(root, 400, 400, Color.BLACK);
        final Stage videoStage = new Stage();
        videoStage.setScene(scene);
        videoStage.setTitle(video.getName());
        videoStage.show();

        player.play();
        player.setOnReady(new Runnable() {
			@Override
			public void run() {
				int h = player.getMedia().getHeight();
				int w = player.getMedia().getWidth();
				
				videoStage.setHeight(h);
				videoStage.setMinWidth(w);
				
				vbox.setMinSize(w, 100);
				vbox.setTranslateY(h - 100);
				
				hbox.setMinSize(w, 50);
				
				slider.setMin(0.0);
				slider.setValue(0.0);
				slider.setMax(player.getTotalDuration().toSeconds());
			}
        });
        player.currentTimeProperty().addListener(new ChangeListener<Duration>() {
            @Override
            public void changed(ObservableValue<? extends Duration> observableValue, Duration duration, Duration current) {
                slider.setValue(current.toSeconds());
            }
        });
        slider.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                player.seek(Duration.seconds(slider.getValue()));
            }
        });
        pauseButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                player.pause();
            }
        });
        playButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                player.play();
            }
        });
        stopButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
            	player.stop();
                videoStage.close();
            }
        });
    }

    /**
     * Returns the data as an observable list of Videos. 
     * @return
     */
    public ObservableList<Video> getVideoData() {
        return videoList;
    }
    
    /**
     * Returns the main stage.
     * @return Stage
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

	public static void main(String[] args) {
		launch(args);
	}
}
